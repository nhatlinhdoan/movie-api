package backend.movieapi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.backend.BackendApplication;
import com.backend.repository.*;
import com.backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.assertj.core.api.Assertions;
import org.junit.After;

@SpringBootTest(classes = BackendApplication.class)
class MovieApiApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
    private UserRepository userRepository;

    @Test
    public void allComponentAreNotNull() {
        Assertions.assertThat(userRepository).isNotNull();
    }
    @Test
    public void testuserRepositoryByCode() {
        userRepository.save(new User("dnlinh","linh doan", "mnhatlinh.doan@gmail.com", "123456"));
        userRepository.save(new User("dnlinh2","linh2 doan", "mnhatlinh2.doan@gmail.com", "123456"));

        Assertions.assertThat(userRepository.findAll()).hasSize(2);
        Assertions.assertThat(userRepository.existsByUsername("dnlinh")).isEqualTo(true);
    }

    @After
    public void clean() {
        userRepository.deleteAll();
    }
}
