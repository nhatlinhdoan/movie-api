package com.backend.controller.user;

import com.backend.exception.ResourceNotFoundException;
import com.backend.model.User;
import com.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api")
public class UserGet {
    @Autowired
    UserRepository userRepository;
    
    @GetMapping("/users/{id}")
    @Cacheable(value = "user-single", key = "#p0")
    public User getUserById(@PathVariable(value = "id") Long userId) {
        System.out.println("__> Getting User by id");
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user", "id", userId));
    }
}
