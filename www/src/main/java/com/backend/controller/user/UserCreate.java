package com.backend.controller.user;

import com.backend.exception.ResourceNotFoundException;
import com.backend.model.User;
import com.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/user")
public class UserCreate {
    @Autowired
    UserRepository userRepository;

    @PostMapping("/register")
    public User createUser(@Valid @RequestBody User user) {
        return userRepository.save(user);
    }
}
