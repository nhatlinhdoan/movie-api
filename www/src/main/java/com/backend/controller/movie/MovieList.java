package com.backend.controller.movie;

import com.backend.exception.ResourceNotFoundException;
import com.backend.model.Movie;
import com.backend.repository.MovieRepository;
import com.backend.payload.movie.*;
import com.backend.payload.*;
import com.backend.service.*;
import com.backend.util.*;

import org.springframework.cache.annotation.Cacheable;
import javax.validation.Valid;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;

@RestController
@RequestMapping("/movie")
public class MovieList {
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieService movieService;

    private static final Logger logger = LoggerFactory.getLogger(MovieList.class);

    @GetMapping("/list")
    public PagedResponse<MovieResponse> list(
        @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
        @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return movieService.listMovies(page, size);
    }

}
