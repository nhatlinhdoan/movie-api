package com.backend.controller.movie;

import com.backend.exception.ResourceNotFoundException;
import com.backend.model.Movie;
import com.backend.service.*;
import com.backend.payload.*;
import com.backend.repository.MovieRepository;
import com.backend.payload.movie.MovieRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/movie")
public class MovieCreate {
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieService movieService;
    
    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> createMovie(@Valid @RequestBody MovieRequest movieRequest) {
        Movie movie = movieService.createMovie(movieRequest);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{movieId}")
                .buildAndExpand(movie.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "Movie Created Successfully"));
    }

}
