package com.backend.controller.movie;

import com.backend.model.Movie;
import com.backend.repository.MovieRepository;
import com.backend.service.*;
import com.backend.payload.movie.MovieResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movie")
public class MovieGet {
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieService movieService;

    private static final Logger logger = LoggerFactory.getLogger(MovieList.class);

    @GetMapping("/get")
    public MovieResponse get(@RequestParam Long id){
        return movieService.getMovie(id);
    } 
}
