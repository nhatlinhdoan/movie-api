package com.backend.model;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.backend.model.audit.DateAudit;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.CreatedDate;
import java.time.Instant;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import lombok.Data;

@Data
@Entity
@Table(name = "backend_movie")
public class Movie extends DateAudit{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(columnDefinition = "nvarchar2(100)")
    private String name;

    @NotBlank
    @Size(max = 100)
    private String ibm;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "backend_movie_type",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "type_id"))
    private List<Type> types = new ArrayList<>();

    @OneToMany(
            mappedBy = "movie",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    @Size(min = 0, max = 10)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 30)
    private List<MovieSource> sources = new ArrayList<>();

    @CreatedDate
    private Instant createdAt;

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getIbm(){
        return this.ibm;
    }

    public void setIbm(String ibm){
        this.ibm = ibm;
    }

    public List<Type> getTypes(){
        return this.types;
    }

    public List<MovieSource> getSources(){
        return this.sources;
    }

    public void addTypes(Type type){
        types.add(type);
    }

    public void removeType(Type type){
        types.remove(type);
    }

    public void addSources(MovieSource source){
        sources.add(source);
    }

    public void removeSources(MovieSource source){
        sources.remove(source);
    }
}
