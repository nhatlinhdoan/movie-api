package com.backend.payload.movie;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TypeResponse {
    @NotBlank
    private String name;

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }
}
