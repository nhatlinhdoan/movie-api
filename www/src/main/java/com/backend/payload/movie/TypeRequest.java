package com.backend.payload.movie;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TypeRequest {
    @NotBlank
    private String name;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
