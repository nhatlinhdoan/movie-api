package com.backend.payload.movie;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import java.util.List;

@Data
public class MovieRequest {
    @NotBlank
    private String name;

    @NotBlank
    private String ibm;

    private List<TypeRequest> types;

    private List<MovieSourceRequest> sources;

    public void setName(String name){
        this.name = name;
    }

    public void setIbm(String ibm){
        this.ibm = ibm;
    }

    public void setTypes(List<TypeRequest> types){
        this.types = types;
    }

    public void setSources(List<MovieSourceRequest> sources){
        this.sources = sources;
    }

    public String getName(){
        return this.name;
    }

    public String getIbm(){
        return this.ibm;
    }

    public List<TypeRequest> getTypes(){
        return this.types;
    }

    public List<MovieSourceRequest> getSources(){
        return this.sources;
    }
}
