package com.backend.payload.movie;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class MovieSourceRequest {
    @NotBlank
    private String name;

    @NotBlank
    private String url;

    public void setName(String name){
        this.name = name;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getName(){
        return this.name;
    }

    public String getUrl(){
        return this.url;
    }
}
