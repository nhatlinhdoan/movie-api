package com.backend.payload.movie;
import java.util.List;
import java.util.ArrayList;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class MovieResponse {
    @NotBlank
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String ibm;

    private List<TypeResponse> types;

    private List<MovieSourceResponse> sources = new ArrayList<>();

    public void setId(Long id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setIbm(String ibm){
        this.ibm = ibm;
    }

    public void setTypes(List<TypeResponse> types){
        this.types = types;
    }

    public void setSources(List<MovieSourceResponse> sources){
        this.sources = sources;
    }

    public Long getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public String getIbm(){
        return this.ibm;
    }

    public List<TypeResponse> getTypes(){
        return this.types;
    }

    public List<MovieSourceResponse> getSources(){
        return this.sources;
    }
}
