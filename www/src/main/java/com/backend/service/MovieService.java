package com.backend.service;
import com.backend.exception.*;
import com.backend.model.*;
import com.backend.payload.movie.*;
import com.backend.payload.PagedResponse;
import com.backend.repository.*;
import com.backend.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieService{
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieSourceRepository movieSourceRepository;

    @Autowired
    private TypeRepository typeRepository;

    private static final Logger logger = LoggerFactory.getLogger(MovieService.class);

    private void validatePageNumberAndSize(int page, int size) {
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
    }

    // Public method
    public PagedResponse<MovieResponse> listMovies(int page, int size){
        validatePageNumberAndSize(page, size);

        Pageable pages = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
        Page<Movie> movies = movieRepository.findAll(pages);

        if(movies.getNumberOfElements() == 0){
            return new PagedResponse<>(
                Collections.emptyList(), movies.getNumber(), movies.getSize(), movies.getTotalElements(), movies.getTotalPages(), movies.isLast()
            );
        }
        List<MovieResponse> movieRepsonses = movies.map(movie -> {
            return ModelMapper.mapMovieToMovieResponse(movie);
        }).getContent();

        return new PagedResponse<>(movieRepsonses, movies.getNumber(), movies.getSize(), movies.getTotalElements(), movies.getTotalPages(), movies.isLast());
    }
    
    public Movie createMovie(MovieRequest movieRequest){
        Movie movie = new Movie();
        movie.setName(movieRequest.getName());
        movie.setIbm(movieRequest.getIbm());

        List<Type> typeList = new ArrayList<>();

        movieRequest.getTypes().forEach(type -> {
            Boolean isTypeExisted = typeRepository.existsByName(type.getName());
            if (isTypeExisted){
                Type _type = typeRepository.findByName(type.getName()).get();
                typeList.add(_type);
            }
            else{
                Type _type = new Type();
                _type.setName(type.getName());
                typeRepository.save(_type);
                typeList.add(_type);
            }
        });
        movie.setTypes(typeList);
        Movie repsonse = movieRepository.save(movie);
        movieRequest.getSources().forEach(_source -> {
            MovieSource source = createSource(_source, repsonse);
            repsonse.addSources(source);
        });
        return repsonse;
    }

    public MovieSource createSource(MovieSourceRequest request, Movie movie){
        MovieSource source = new MovieSource();
        source.setName(request.getName());
        source.setUrl(request.getUrl());
        source.setMovie(movie);

        return movieSourceRepository.save(source);
    }

    public MovieResponse getMovie(Long id){
        Movie movie = movieRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Movie", "id", id));
        
        return ModelMapper.mapMovieToMovieResponse(movie);
    }
}