package com.backend.util;
import com.backend.model.*;
import com.backend.payload.movie.*;
import java.util.List;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.stream.Collectors;

public class ModelMapper{
    private static final Logger logger = LoggerFactory.getLogger(ModelMapper.class);

    public static MovieResponse mapMovieToMovieResponse(Movie movie){
        MovieResponse movieResponse = new MovieResponse();
        List<TypeResponse> typeReponses = movie.getTypes().stream().map(type -> {
            return mapTypeToTypeResponse(type);
        }).collect(Collectors.toList());
        List<MovieSourceResponse> sources = movie.getSources().stream().map(source -> {
            return mapMovieSourceToMovieSourceResponse(source);
        }).collect(Collectors.toList());

        movieResponse.setId(movie.getId());
        movieResponse.setIbm(movie.getIbm());
        movieResponse.setName(movie.getName());
        movieResponse.setTypes(typeReponses);
        movieResponse.setSources(sources);
        return movieResponse;
    }

    public static TypeResponse mapTypeToTypeResponse(Type type){
        TypeResponse typeResponse = new TypeResponse();
        typeResponse.setName(type.getName());
        return typeResponse;
    }

    public static MovieSourceResponse mapMovieSourceToMovieSourceResponse(MovieSource source){
        MovieSourceResponse movieSourceResponse = new MovieSourceResponse();
        movieSourceResponse.setName(source.getName());
        movieSourceResponse.setUrl(source.getUrl());
        return movieSourceResponse;
    }
}