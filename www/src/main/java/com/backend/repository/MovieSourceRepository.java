package com.backend.repository;

import com.backend.model.MovieSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieSourceRepository extends JpaRepository<MovieSource, Long> {
    List<MovieSource> findByMovieId(Long id);

    Long deleteByMovieId(Long id);
}
