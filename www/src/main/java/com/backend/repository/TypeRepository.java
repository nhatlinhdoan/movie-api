package com.backend.repository;

import com.backend.model.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {
    List<Type> findByIdIn(List<Long> TypeIds);
    Optional<Type> findByName(String name);
    Boolean existsByName(String name);
}
