# MOVIE API RESTFUL

## INTRO
Dạ thưa anh, 
Hiện em chỉ làm được 1 xíu tới đây (được trình bày ở dưới) và chỉ viết được 1 Unit test.
Tuy nhiên trong quá trình làm em đã có cái nhìn tổng quan và cụ thể về java cũng như spring boot, và vẫn đang tiếp tục học và tìm hiểu đào sâu trong khoảng thời gian ít ỏi ngoài giờ làm.
Bên cạnh đó project này của em vẫn tiếp tục được build và maintance vào thời gian rảnh của em.
Mong anh có gì bỏ qua cho những thiếu sót của em. Em chân thành cảm ơn anh

## Prerequisite
- Linux OS (recommended Ubuntu for beginer, i'm using Arch Linux)
- Docker ce
- Oracle sql developer (recommended Dbeaver)
- Java jdk 1.8

## Tools
- spring boot
- oracle db
- spring cli / <https://start.spring.io/>
- oracle jdcbc
- flyway
- spring data jpa
- spring devtools
- spring rest-docs
- build-essential (for run Makefile on linux)

## Currenly Status
- Not yet build on docker
- Not yet write unit test
- Not yet seperate Spring Profile for each environment


## Spring Boot Aproaching

### Hierachy of Collection in Java
[!Collection](https://static.javatpoint.com/images/java-collection-hierarchy.png)

### Tigh Coupling && Lossely coupled

### Dependency Injection (SOLID principle) && IoC (Inversion of Control)
    - Dependency Injection is a technique whereby one object supplies the dependencies of another object. A "dependency" is an object that can be used, for example as a service. Instead of a client specifying which service it will use, something tells the client what service to use. The "injection" refers to the passing of a dependency (a service) into the object (a client) that would use it. The service is made part of the client's state.[1] Passing the service to the client, rather than allowing a client to build or find the service, is the fundamental requirement of the pattern.
    - Inversion of Control is a programming principle. flow of control within the application is not controlled by the application itself, but rather by the underlying framework.
    - ApplicationContext isContainer which include all of dependency
    - Bean is Dependency

### @Configuration và @Bean
- @Bean is a method-level annotation and a direct analog of the XML <bean/> element. The annotation supports most of the attributes offered by <bean/>, such as: init-method, destroy-method, autowiring, lazy-init, dependency-check, depends-on and scope.

### @Component
- Markable anntiontion helps Spring will understand the class which have @Component is a Bean(Dependency) and Spring Boot will manage this.

### @Autowired

### Controller - Service - Repository
![controller service repository](https://loda.me/assets/static/2.3eafece.9c09a32.png)

### @RestControllerAdvice & @ControllerAdvice + @ExceptionHandler

### @ConfigurationProperties

### @RestController @RequestBody @PathVariable

### Spring Profile - @Profile

### Hibernate
- @ManyToOne & OneToMany
    @EqualsAndHashCode.Exclude // not using in equals and hashcode
    @ToString.Exclude // NOT using in case has toString()
    fetch = FetchType.LAZY // only query when needed
- @ManyToMany
- @OneToOne
    - @JoinColumn

### @SpringBootTest


### Security
- @EnableWebSecurity: the primary spring security annotation that is used to enable web security in a project.
- securedEnabled: It enables the @Secured annotation using which you can protect your controller/service methods
    ```java
    @Secured("ROLE_ADMIN")
    public User getAllUsers() {}
    ```
- jsr250Enabled: It enables the @RolesAllowed annotation that can be used like this
    ```java
    @RolesAllowed("ROLE_ADMIN")
    public Poll createPoll() {}  
    ```

- prePostEnabled: It enables more complex expression based access control syntax with @PreAuthorize and @PostAuthorize annotations -
```java
    @PreAuthorize("isAnonymous()")
    public boolean isUsernameAvailable() {} 
```
- WebSecurityConfigurerAdapter: This class implements Spring Security’s WebSecurityConfigurer interface. It provides default security configurations and allows other classes to extend it and customize the security configurations by overriding its methods.
- CustomUserDetailsService
To authenticate a User or perform various role-based checks, Spring security needs to load users details somehow.
For this purpose, It consists of an interface called UserDetailsService which has a single method that loads a user based on username-
```java
UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
```
- JwtAuthenticationEntryPoint
This class is used to return a 401 unauthorized error to clients that try to access a protected resource without proper authentication. It implements Spring Security’s AuthenticationEntryPoint interface.
- JwtAuthenticationFilter to implement a filter that -
    - reads JWT authentication token from the Authorization header of all the requests
    - validates the token
    - loads the user details associated with that token.
    - Sets the user details in Spring Security’s SecurityContext. Spring Security uses the user details to perform authorization checks. We can also access the user details stored in the SecurityContext in our controllers to perform our business logic.
    - customUserDetailsService and a passwordEncoder to build the AuthenticationManager. --> AuthenticationManager to authenticate a user in the login API.
    - The HttpSecurity configurations are used to configure security functionalities like csrf, sessionManagement, and add rules to protect resources based on various conditions.


## Description

### Oauth2 in service
![Oauth2 Diagram](http://www.bubblecode.net/wp-content/uploads/2013/03/auth_code_flow.png)
- The basic OAuth2 defines 4 roles :
    - **Resource Owner**: generally yourself.
    - **Resource Server**: server hosting protected data (for example Google hosting your profile and personal information).
    - **Client**: application requesting access to a resource server (it can be your PHP website, a Javascript application or a mobile application).
    - **Authorization Server**: server issuing access token to the client. This token will be used for the client to request the resource server. This server can be the same as the authorization server (same physical server and same application), and it is often the case.

![Oauth2 for my project](https://i.stack.imgur.com/zI0Gp.png)
---> First Step Security: Basic configure, all in one API
- *Authorization* grant type **Implicit Grant**:
    - **Resource Owner** : user of all types in DB
    - **Resource Server**: RESTFul Spring Boot API Service (Backend)
    - **Client**: ReactJS + redux (Frontend)
    - **Authorization Server**: RESTFul Spring Boot API Service (Backend)

- Scenario:
    - The client (ReactJS) wants to obtain information about your **Backend**'s data.
    - You are redirected by the browser to the login page.
    - Form will submit data to **Backend**
    - If you authorize access, the authorization server - **Backend** send to access token to **Frontend**
    - The **Frontend** store this access token to cookie and use it to retrieve the **Backend**'s data
    - The access token have exp date (30 days) --> after 3 days client has to request to grant access token again.

---> Next Step Advance Security: Centralize dependency authentication service - Microservice OneLogin
- *Authorization* grant type **Implicit Grant**:
    - **Resource Owner** : user of all types in DB
    - **Resource Server**: RESTFul Spring Boot API Service (Backend)
    - **Client**: ReactJS + redux (Frontend)
    - **Authorization Server**: MicroService OneLogin (because of it for later build so i will consider carefully the framework)

- Scenario:
    - The client (ReactJS) wants to obtain information about your **Backend**'s data.
    - You are redirected by the browser to the login page of **MicroService OneLogin**
    - If you authorize access, the authorization server - **MicroService OneLogin** redirect to URI with access token
    - The **Frontend** store this access token to cookie and use it to retrieve the **Backend**'s data
    - The access token have exp date (30 days) --> after 3 days client has to request to grant access token again.

### Structure of project
**K.I.S.S** *Keep It Simple, Stupid / Straiforward* Principle
- com.backend
    - config
        - Audit config for model && middleware for api
    - controller
        - `Devide and conquer`
        - Route of api
        - every route will be contented in 1 one, code will be duplicated but easy to scale and maintain
        - i.e :
            - user
                - get
                - list
                - login
                - update
                - delete
    - exception
        - Exception data of api
    - model
        - representation of table in db
    - payload
        - `Devide and conquer`
        - Define specific request data structure
    - repository
    - security
    - util
- resources
    - application.properties (application.yml will be better readable but im to busy and lazy): config of spring boot

## Init
![Implicit Grant](http://www.bubblecode.net/wp-content/uploads/2013/03/implicit_flow.png)

## How I Init the project

```bash
spring init --build=maven --name=movie-api --groupId=backend --java-version=1.8 --dependencies=web,oracle,devtools,data-jpa,flyway,restdocs,session --packaging=jar movie-api
```

## How to run

- run oracle db docker
![Oracle Database and Instance](https://docs.oracle.com/database/121/CNCPT/img/GUID-A30AF38A-7D55-4987-A0FB-02BAAF079AC5-default.gif)

```bash
sudo docker run -d --restart=always --name localbox.oracle12e \
-p 1569:1521 -p 5569:5500 \
store/oracle/database-enterprise:12.2.0.1
```
- login to oracle system db to create new user
  - username:                                                           SYSTEM
  - The default password to connect to the database with sys user is:   Oradoc_db1 .
  - SID:                                                                ORCLCDB
- create new user - schema for oracle db
```sql
CREATE USER C##MOVIE_API IDENTIFIED BY "Dnlinh1710!";
GRANT CONNECT, RESOURCE, DBA TO C##MOVIE_API;
GRANT ALL privileges TO C##MOVIE_API;
GRANT UNLIMITED TABLESPACE TO C##MOVIE_API;
```

### Create user
POST http://localhost:8896/api/auth/signup
BODY
```
{
	"username" : "dnlinh",
	"email" : "mnhatlinh.doan@gmail.com",
	"name": "dnlinh",
	"password": "123456"
}
```

### Login
POST http://localhost:8896/api/auth/signin
BODY
```
{
	"usernameOrEmail" : "dnlinh",
	"password": "123456"
}

--> get authorization key: 
```

### Create movie
- Need to add one record in DB 
```sql
INSERT INTO "C##MOVIE_API".BACKEND_USER_ROLE(user_id, role_id) VALUES (1, 2) ;
```
- POST http://localhost:8896/api/movie/create
HEADERS Authorization Bearer <YOUR-AUTHORIZATION>
BODY
```
{
	"name" : "AVenger",
	"ibm" : "8",
	"types": [{"name" : "action"}, {"name": "funny"}, {"name" : "drama"}],
	"sources":[
		{"name": "usa", "url": "http://ddddddd/hydrax.html#slug=rrhRVnU6R"}
	]
}
```

### List movie
GET http://localhost:8896/movie/list


### Get movie
GET http://localhost:8896/movie/get?id=1
---
- cd to project dir
- for local dev:
```bash
    make run
```
- for build and deploy production:
```bash
    make build
    make deploy
```

## References
https://blog.marcosbarbero.com/centralized-authorization-jwt-spring-boot2/
https://github.com/callicoder/spring-security-react-ant-design-polls-app



{
	"name" : "ĐẠI CHIẾN THẾ GIỚI NGẦM",
	"ibm" : "8",
	"types": [{"name" : "action"}, {"name": "funny"}, {"name" : "drama"}],
	"sources":[
		{"name": "server 1", "url": "http://www.phimmoi.link/hydrax.html#slug=rrhRVnU6R"}
	]
}

