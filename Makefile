check:
	cd www && mvn dependency:tree

run:
	cd www && mvn spring-boot:run -X

clean:
	cd www && mvn clean install

build:
	sudo docker build -f docker/Dockerfile-dev -t movie-api .

deploy:
	sudo docker run -d --name=movie.api \
		-v www:/app \
		-p 8979:8080 \
		--link localbox.oracle12e:localoracle \
		movie-api

clear:
	-sudo docker stop movie.api
	-sudo docker rm movie.api